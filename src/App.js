import Container from 'react-bootstrap/Container'
import { BrowserRouter as Router } from 'react-router-dom';
import { Route, Routes } from 'react-router-dom';
import { useState, useEffect } from 'react';

import AppNavbar from './components/AppNavbar';
import Home from './pages/Home';
import Register from './pages/Register';
import Login from './pages/Login';
import Logout from './pages/Logout';
import Profile from './pages/Profile';


import AddProduct from './pages/AddProduct';
import Products from './pages/Products';
import ProductView from './pages/ProductView';

import Error from './pages/Error';

import './App.css';
import { UserProvider } from './UserContext';

function App() {

  const [user, setUser] = useState({
    //token: localStorage.getItem('token')
    id: null,
    isAdmin: null
  })

  const unsetUser = () => {
    localStorage.clear();
  }

  // Used to check if the user information is properly stored upon login and the localStorage information is cleared upon logout
  useEffect(() => {
    //console.log(user);
    //console.log(localStorage);
    fetch(`https://cpstn2-ecommerce-ordanez.onrender.com/users/details`,{
      headers: {
        Authorization: `Bearer ${ localStorage.getItem('token')}`
      }
    })
    .then(res => res.json())
    .then(data => {
      console.log(data)
      console.log("App.js")
      // Set the user state values with the user details upon successful login.
      if (typeof data._id !== "undefined") {
        setUser({
          id: data._id,
          isAdmin: data.isAdmin
        })
      }
      else{
        setUser({
          id: null,
          isAdmin: null
        })
      } 
    })
  }, [user])

  return (
    // React Fragments <></>
    <UserProvider value={{ user, setUser, unsetUser}}>
    <Router>
      
      <Container fluid>

        <AppNavbar />

        <Routes>
          <Route path="/" element={<Home/>} />
          <Route path="/register" element={<Register/>} />
          <Route path="/login" element={<Login/>} />
          <Route path="/logout" element={<Logout/>} />
          <Route path="/profile" element={<Profile />} />
          
          <Route path="/products" element={<Products/>} />
          <Route path ="/addProduct" element={<AddProduct/>}/>
          <Route path="/products/:productId" element={<ProductView />}/>
          <Route path="*" element={<Error/>} />

        </Routes>

      </Container>
    </Router> 
    </UserProvider>
  );
}

export default App;
