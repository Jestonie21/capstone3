import React, { useState, useEffect } from 'react';
import ProductCard from './ProductCard';


export default function UserView({productsData}) {

	const [products, setProducts] = useState([])

  useEffect(() => {
    const fetchDataForUserView = async () => {
      try {
        const response = await fetch('https://cpstn2-ecommerce-ordanez.onrender.com/products', {
          method: 'GET',
          headers: {
            'Content-Type': 'application/json',
          },
        });

        if (!response.ok) {
          throw new Error(`HTTP error! Status: ${response.status}`);
        }

        const data = await response.json();
        
        const activeProducts = data.filter((product) => product.isActive === true);
        
        const productCards = activeProducts.map((product) => (
          <ProductCard productProp={product} key={product._id} />
        ));

        setProducts(productCards);
      } catch (error) {
      }
    };

    fetchDataForUserView();
  }, []);

	return(
		<>
			{ products }
		</>
		)
}