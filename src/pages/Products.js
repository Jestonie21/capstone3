import { useEffect, useState, useContext } from 'react';
import ProductCard from '../components/ProductCard';
import UserContext from '../UserContext';
import UserView from '../components/UserView';
import AdminView from '../components/AdminView';


export default function Products() {

	const { user } = useContext(UserContext);

	const [products, setProducts] = useState([]);

	const fetchData = async () => {
	  	try {
	    const response = await fetch(`https://cpstn2-ecommerce-ordanez.onrender.com/products/all`, {
	      method: 'GET',
	      headers: {
	        'Content-Type': 'application/json',
	        Authorization: `Bearer ${localStorage.getItem('token')}`,
	      },
	    });

	    if (!response.ok) {
	      throw new Error(`HTTP error! Status: ${response.status}`);
	    }

	    const data = await response.json();
	    setProducts(data);
	    
	  } catch (error) {
	    console.error('Error fetching product data:', error);
	  }
	};

	//console.log(products)

	useEffect(() => {
		fetchData()
	}, [])


	return(
		<>
		    {
		    	(user.isAdmin === true) 
		    		?
		    		<AdminView productsData={products} fetchData={fetchData}/>
		    		:
		    		<UserView productsData={products} />
        	}
		    		
		</>
	)

};