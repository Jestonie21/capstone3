import Banner from '../components/Banner'


export default function Home() {


const data = {
    title: "JF Shop",
    content: "Welcome to the JF Shop and discover the latest technology! Indulge in the newest ways to get in touch or find the model for your gaming needs right at the tip of your fingers.",
    destination: "/products",
    label: "Shop now!"
}

	return (
		<>
			<Banner data={data} />

		</>
		
	)
}