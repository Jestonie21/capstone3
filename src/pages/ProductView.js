import { useState, useEffect, useContext } from 'react';
import { Container, Card, Button, Row, Col } from 'react-bootstrap';
import { useParams, useNavigate, Link } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';


export default function ProductView() {

	const { user } = useContext(UserContext);
	const navigate = useNavigate();

	const { productId} = useParams();

	const [ name, setName ] = useState("");
	const [ description, setDescription ] = useState("");
	const [ price, setPrice ] = useState(0);

	const order = (productId) => {
		fetch(`https://cpstn2-ecommerce-ordanez.onrender.com/orders/checkout`, {
			method: 'POST',
			headers: {
				"Content-Type": "application/json",
				Authorization: `Bearer ${ localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				productId: productId
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);

			if (data.message = 'Order Successfully.'){
				Swal.fire({
					title: "Successfully Purchased!",
					icon: 'success',
					text: "Thank you for buying!"
				})
			}
			else{
				Swal.fire({
					title: "Error",
					icon: 'error',
					text: "Please try again."
				})
			}
		})
	}

	useEffect(() => {
		console.log(productId);

		fetch(`https://cpstn2-ecommerce-ordanez.onrender.com/products/${productId}`)
		.then(res => res.json())
		.then(data => {
			console.log(data);

			setName(data.name);
			setDescription(data.description)
			setPrice(data.price);
		})
	}, [productId])

	return(
		<Container className="mt-5">
			<Row>
				<Col lg={{ span: 6, offset: 3}}>
					<Card.Body className="text-center">
						<Card.Title>{name}</Card.Title>
						<Card.Subtitle>Description:</Card.Subtitle>
						<Card.Text>{description}</Card.Text>
						<Card.Subtitle>Price</Card.Subtitle>
						<Card.Text>₱ {price}</Card.Text>
						<Card.Subtitle></Card.Subtitle>
						<Card.Text></Card.Text>
						{ user.id !== null ?
							<Button variant="primary" block onClick={() => order(productId)}>Order Now!</Button>
							:
							<Link className="btn btn-danger btn-block" to="/login">Log in to Order</Link>
						}
												
					</Card.Body>
				</Col>
			</Row>
		</Container>
	);
}
