# E-COMMERCE APPLICATION

## Features:
- Register Page

- Login Page

- Admin Dashboard
    - Create product
    - Retrieve all products
    - Update product information
    - Deactivate/Reactivate product

- User Catalog Page
    - View all active product
    - Retrieve single product

- Checkout Order
    - Non-admin User checkout(Create Order)

<details>
<summary>
TEST ACCOUNTS
</summary>

- Test User:
    - email: nonadmin@gmail.com
    - pwd: 12345

- Admin User:
    - email: admin@gmail.com
    - pwd: 12345
</details>